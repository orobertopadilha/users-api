import 'express-async-errors'
import express from 'express'
import cors from 'cors'
import { errorHandler } from './middleware'
import userRouter from './routes/user'
import authRouter from './routes/auth'

const app = express()
app.use(express.json())

app.use(cors())
app.use('/user', userRouter)
app.use('/auth', authRouter)

errorHandler(app)

app.listen(9091, () => console.log('User API is up!'))