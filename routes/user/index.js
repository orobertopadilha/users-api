import express from 'express'
import { userService } from '../../domain/service'

const userRouter = express.Router()

userRouter.post('/', async (req, res) => {
    await userService.save(req.body)
    res.json({})
})

userRouter.get('/', async (req, res) => {
    const users = await userService.findAll()

    if (users.length === 0) {
        res.status(204)
        res.end()
    } else {
        res.json(users)
    }
})

userRouter.get('/me', async (req, res) => {
    const user = await userService.findById(req.header('user-auth-id'))
    res.json(user)
})

export default userRouter