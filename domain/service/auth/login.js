import { userRepository } from "../../../data/repository"
import { AuthException } from "../../../exception"
import { validateLogin } from "../../validators/login"
import { authHelper } from 'auth-lib'

import bcrypt from 'bcrypt'

export const login = async (loginData) => {
    validateLogin(loginData.login, loginData.password)

    let user = await userRepository.findByEmail(loginData.login)

    if (user != null) {
        if (bcrypt.compareSync(loginData.password, user.password)) {
            let token = authHelper.createToken(user)
            
            return { 
                id: user.id,
                name: user.name,
                token
            }
        }
    }

    throw new AuthException('Dados inválidos para o login!')
}