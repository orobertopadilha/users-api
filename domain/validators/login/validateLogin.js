import { ValidationException } from "../../../exception"

export const validateLogin = (login, password) => {

    if (login == null || login === '') {
        throw new ValidationException('Login é obrigatório!')
    }

    if (password == null || password === '') {
        throw new ValidationException('Senha é obrigatória!')
    }

}